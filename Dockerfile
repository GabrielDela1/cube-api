# FROM node:16 AS api

# # RUN apt-get update -yq \
# # && apt-get install curl gnupg -yq \
# # && curl -sL https://deb.nodesource.com/setup_18.x | bash \
# # && apt-get install nodejs -yq \
# # && apt-get clean -y

# WORKDIR /usr/src/app

# COPY ["package.json", "package-lock.json*", "/usr/src/app/"]

# #COPY package*.json ./
# # COPY package*.json ./app

# RUN file="$(ls -1 /usr/src/app/)" && echo $file
# RUN echo $(ls -1 /usr/src/app/)

# RUN npm install

# COPY . .

# EXPOSE 5000

# CMD ["npm", "run", "start"]

# # RUN npm run build

# # COPY docker/nodejs/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
# # RUN chmod +x /usr/local/bin/docker-entrypoint.sh

# # ENTRYPOINT ["docker-entrypoint.sh"]
# # CMD npm run start

FROM node:16 as api

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# RUN --mount=target=/public,type=bind,source=public/

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]
require('dotenv').config();
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const AuthRoutes = require('./routes/auth');
const UserRoutes = require('./routes/users');
const ResourceRoutes = require('./routes/resources');
const CommentRoutes = require('./routes/comments');
const CategoryRoutes = require('./routes/categories');
const TypeRoutes = require('./routes/types');
const { urlencoded } = require('body-parser');
const RelationsRoutes = require('./routes/relations');

const AdminUsersRoutes = require('./routes/admin/users');
const AdminStatusRoutes = require('./routes/admin/status');
const AdminRolesRoutes = require('./routes/admin/roles');
const AdminCategoriesRoutes = require('./routes/admin/categories');
const AdminTypesRoutes = require('./routes/admin/types');
const AdminResourceRoutes = require('./routes/admin/resources');
const AdminCommentsRoutes = require('./routes/admin/comments');



// export one function that gets called once as the server is being initialized
module.exports = function (app, server) {
    const mongoose = require('mongoose');

    let url = `mongodb://${process.env.DB_URL}/cube?retryWrites=true&w=majority`;

    mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_URL}/cube?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log('DB is OK');
    })
    .catch((e) => {
        console.log('DB failed', e);
    });

    app.use(express.json());
    app.use(urlencoded({ extended: true }));
    app.use(bodyParser.json({ limit: '100mb' }));

    app.use(express.static('public'));
    app.use('/images', express.static('images'));

    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', '*');
        res.setHeader('Access-Control-Allow-Methods', '*');
        next();
    });

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    app.use('/api/auth', AuthRoutes);
    app.use('/api/users', UserRoutes);
    app.use('/api/comments', CommentRoutes);
    app.use('/api/resources', ResourceRoutes);
    app.use('/api/categories', CategoryRoutes);
    app.use('/api/types', TypeRoutes);
    app.use('/api/relations', RelationsRoutes);
    
    app.use('/api/admin/users', AdminUsersRoutes);
    app.use('/api/admin/status', AdminStatusRoutes);
    app.use('/api/admin/roles', AdminRolesRoutes);
    app.use('/api/admin/categories', AdminCategoriesRoutes);
    app.use('/api/admin/types', AdminTypesRoutes);
    app.use('/api/admin/resources', AdminResourceRoutes);
    app.use('/api/admin/comments', AdminCommentsRoutes);
}
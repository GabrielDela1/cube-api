const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
    id_resource: { type: String, required: true },
    id_user: { type: String, required: true },
    comment: { type: String, required: true },
    deleted: { type: Boolean, required: false, default: false},
    create_date: {type: Date, required: true },
});

module.exports = mongoose.model('Comment', commentSchema)
const mongoose = require('mongoose');

const relationSchema = mongoose.Schema({
    id_from: { type: String, required: true },
    id_to: { type: String, required: true },
    relation: { type: String, required: true, default: "Ami" },
});

module.exports = mongoose.model('Relation', relationSchema)
const mongoose = require('mongoose');

const resourceSchema = mongoose.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    image: { type: String, required: true },
    content: { type: String, required: true },
    user_id: { type: String, required: true },
    likes: { type: Number, required: false, default: 0 },
    share: { type: Number, required: false, default: 0 },
    category_id : { type: String, required: false, default: null },
    type_id : { type: String, required: false, default: null },
    created_at: { type: Date, required: true },
    updated_at: { type: Date, required: false },
    status : { type: String, required: false, default: 'waiting' },
});

module.exports = mongoose.model('Resource', resourceSchema)
const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    tag : { type: String, required: true },
    biography: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String, required: true },
    google_id: { type: String, required: false },
    avatar: { type: String, required: false },
    age : { type: Number, required: true },
    role : { type: String, required: true, default: 'user' },
    status: { type: String, required: true, default: 'activated' },
    favorites: [{ type: String, required: false }],
});

module.exports = mongoose.model('User', userSchema);
var express = require('express');
var router = express.Router();
const Category = require('../../models/category');
const bodyParser = require('body-parser');
const sha256 = require('js-sha256');
require('dotenv').config();

const auth = require('../isAuth.js');
const admin = require('../isAdmin.js');

router.get('/', admin, function (req, res) { // auth to add
    try {
        Category.find().then(data => {
            return res.status(200).json(data);
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.get('/:id', admin, function (req, res) {
    try {
        if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
            Category.findOne({ _id: req.params.id })
                .then(category => {
                    res.status(200).json(category);
                })
                .catch(error => res.status(404).json({ error }));
        }
        else {
            res.status(404).json('Invalid category ID');
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.post('/', admin, function (req, res) {
    var data = req.body;
    if (data.name) {
        const category = new Category({
            name: data.name,
        });

        category.save().then(() => {
            res.status(200).json({ message: 'Category registered' });
        }).catch((error) => {
            res.status(400).json({ error });
        });
    }
    else {
        res.status(404).json('Invalid role model');
    }
});

router.patch('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Category.findOne({ _id: req.params.id })
            .then(category => {
                for (var field in req.body) {
                    if (category[field] != null) {
                        category[field] = req.body[field];
                    }
                }

                category.save().then(() => {
                    res.status(200).json({ message: 'Category updated', category: category });
                }).catch((error) => {
                    res.status(400).json({ error });
                });
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid category ID');
    }
});

router.delete('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Category.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Category deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid category ID');
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();
const Comment = require('../../models/comment');
const bodyParser = require('body-parser');
const sha256 = require('js-sha256');
require('dotenv').config();

const auth = require('../isAuth.js');
const admin = require('../isAdmin.js');

router.get('/', admin, function (req, res) { // auth to add
    try {
        Comment.find().then(data => {
            return res.status(200).json(data);
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.get('/:id', admin, function (req, res) {
    try {
        if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
            Comment.findOne({ _id: req.params.id })
                .then(comment => {
                    res.status(200).json(comment);
                })
                .catch(error => res.status(404).json({ error }));
        }
        else {
            res.status(404).json('Invalid comment ID');
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
});


router.patch('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Comment.findOne({ _id: req.params.id })
            .then(comment => {
                for (var field in req.body) {
                    if (comment[field] != null) {
                        comment[field] = req.body[field];
                    }
                }

                comment.save().then(() => {
                    res.status(200).json({ message: 'Comment updated', comment: comment });
                }).catch((error) => {
                    res.status(400).json({ error });
                });
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid comment ID');
    }
});

router.delete('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Comment.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Comment deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid comment ID');
    }
});

module.exports = router;
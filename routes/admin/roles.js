var express = require('express');
var router = express.Router();
const Role = require('../../models/role');
const admin = require('../isAdmin.js');

router.get('/', admin, function (req, res) { // auth to add
    try {
        Role.find().then(data => {
            return res.status(200).json(data);
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.get('/:id', admin, function (req, res) {
    try {
        if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
            Role.findOne({ _id: req.params.id })
                .then(role => {
                    res.status(200).json(role);
                })
                .catch(error => res.status(404).json({ error }));
        }
        else {
            res.status(404).json('Invalid role ID');
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.post('/', admin, function (req, res) {
    var data = req.body;
    if (data.name) {
        const role = new Role({
            name: data.name,
        });

        role.save().then(() => {
            res.status(200).json({ message: 'Role registered' });
        }).catch((error) => {
            res.status(400).json({ error });
        });
    }
    else {
        res.status(404).json('Invalid role model');
    }
});

router.patch('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Role.findOne({ _id: req.params.id })
            .then(role => {
                for (var field in req.body) {
                    if (role[field] != null) {
                        role[field] = req.body[field];
                    }
                }

                role.save().then(() => {
                    res.status(200).json({ message: 'Role updated', role: role });
                }).catch((error) => {
                    res.status(400).json({ error });
                });
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid role ID');
    }
});

router.delete('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Role.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Role deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid role ID');
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();
const Status = require('../../models/status');
const admin = require('../isAdmin.js');

router.get('/', admin, function (req, res) { // auth to add
    try {
        Status.find().then(data => {
            return res.status(200).json(data);
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.get('/:id', admin, function (req, res) {
    try {
        if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
            Status.findOne({ _id: req.params.id })
                .then(status => {
                    res.status(200).json(status);
                })
                .catch(error => res.status(404).json({ error }));
        }
        else {
            res.status(404).json('Invalid status ID');
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.post('/', admin, function (req, res) {
    var data = req.body;
    if (data.name) {
        const status = new Status({
            name: data.name,
        });

        status.save().then(() => {
            res.status(200).json({ message: 'Status registered' });
        }).catch((error) => {
            res.status(400).json({ error });
        });
    }
    else {
        res.status(404).json('Invalid status model');
    }
});

router.patch('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Status.findOne({ _id: req.params.id })
            .then(status => {
                for (var field in req.body) {
                    if (status[field] != null) {
                        status[field] = req.body[field];
                    }
                }

                status.save().then(() => {
                    res.status(200).json({ message: 'Status updated', status: status });
                }).catch((error) => {
                    res.status(400).json({ error });
                });
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid status ID');
    }
});

router.delete('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Status.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Status deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid status ID');
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();
const Type = require('../../models/type');
const bodyParser = require('body-parser');
const sha256 = require('js-sha256');
require('dotenv').config();

const auth = require('../isAuth.js');
const admin = require('../isAdmin.js');

router.get('/', admin, function (req, res) { // auth to add
    try {
        Type.find().then(data => {
            return res.status(200).json(data);
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.get('/:id', admin, function (req, res) {
    try {
        if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
            Type.findOne({ _id: req.params.id })
                .then(type => {
                    res.status(200).json(type);
                })
                .catch(error => res.status(404).json({ error }));
        }
        else {
            res.status(404).json('Invalid type ID');
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.post('/', admin, function (req, res) {
    var data = req.body;
    if (data.name) {
        const type = new Type({
            name: data.name,
        });

        type.save().then(() => {
            res.status(200).json({ message: 'Type registered' });
        }).catch((error) => {
            res.status(400).json({ error });
        });
    }
    else {
        res.status(404).json('Invalid role model');
    }
});

router.patch('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Type.findOne({ _id: req.params.id })
            .then(type => {
                for (var field in req.body) {
                    if (type[field] != null) {
                        type[field] = req.body[field];
                    }
                }

                type.save().then(() => {
                    res.status(200).json({ message: 'Type updated', type: type });
                }).catch((error) => {
                    res.status(400).json({ error });
                });
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid type ID');
    }
});

router.delete('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Type.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Type deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid type ID');
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();
const User = require('../../models/user');
const bodyParser = require('body-parser');
const sha256 = require('js-sha256');
require('dotenv').config();

const auth = require('../isAuth.js');
const admin = require('../isAdmin.js');

router.get('/', admin, function (req, res) { // auth to add
    try {
        User.find().then(data => {
            return res.status(200).json(data);
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.get('/:id', admin, function (req, res) {
    try {
        if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
            User.findOne({ _id: req.params.id })
                .then(user => {
                    res.status(200).json(user);
                })
                .catch(error => res.status(404).json({ error }));
        }
        else {
            res.status(404).json('Invalid user ID');
        }
    }
    catch (err) {
        return res.status(500).json(err);
    }
});

router.post('/', admin, function (req, res) {
    var data = req.body;
    if (data.firstname != null && data.lastname != null && data.email != null) {
        if (data.password != null || data.google_id != null) {

            var tag = '@' + data.firstname.substring(0, 2) + data.lastname.substring(0, 2) + Math.floor(Math.random() * (1000 - 100 + 1)) + 100;
            var password = data.password != null ? sha256(data.password) : null;

            const user = new User({
                firstname: data.firstname,
                lastname: data.lastname,
                tag: tag,
                biography: data.biography,
                email: data.email,
                password: password,
                google_id: data.google_id,
                avatar: "https://robohash.org/" + data.firstname.substring(0, 2) + data.lastname.substring(0, 2) + Math.floor(Math.random() * (1000 - 100 + 1)) + 100,
                age: data.age
            });
            user.save().then(() => {
                res.status(200).json({ message: 'User registered' });
            }).catch((error) => {
                res.status(400).json({ error });
            });
        }
        else {
            res.status(404).json('Invalid user model');
        }
    }
    else {
        res.status(404).json('Invalid user model');
    }
});

router.patch('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.findOne({ _id: req.params.id })
            .then(user => {
                for (var field in req.body) {
                    if (user[field] != null) {
                        user[field] = req.body[field];
                    }
                }

                user.save().then(() => {
                    res.status(200).json({ message: 'User updated', user: user });
                }).catch((error) => {
                    res.status(400).json({ error });
                });
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});

router.delete('/:id', admin, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'User deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});

module.exports = router;
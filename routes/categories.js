var express = require('express');
var router = express.Router();
const Category = require('../models/category');
const auth = require('../routes/isAuth.js');

router.get('/', auth, function (req, res) {
    Category.find().then(data => {
        res.status(200).json(data);
    });
});

router.post('/', auth, function (req, res) {
    const category = new Category({
        name: req.body.name,
    });
    category.save().then(data => {
        res.status(200).json(data);
    });
});

module.exports = router;
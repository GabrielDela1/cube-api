var express = require('express');
var router = express.Router();
const Comment = require('../models/comment');
const Resource = require('../models/resource');
const User = require('../models/user');
const auth = require('../routes/isAuth.js');

router.get('/', auth, function (req, res) {
    Comment.find().then(data => {
        res.status(200).json(data);
    });
});

router.post('/', auth, function (req, res) {
    var data = req.body;
    if (data.id_resource != null && data.id_user != null && data.comment != null) {
        const comment = new Comment({ id_resource: data.id_resource, id_user: data.id_user, comment: data.comment, create_date: Date.now() });
        comment.save().then(() => {
            res.status(201).json({ message: 'Comment registered' });
        }).catch((error) => {
            res.status(400).json({ error });
        });
    }
    else {
        res.status(404).json('Invalid comment model');
    }
});

// get comment by ressource
router.get('/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Comment.find({ id_resource: req.params.id })
            .then(data => {
                res.status(200).json(data);
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid resource ID');
    }
});

router.get('/latest/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Comment.find({ id_resource: req.params.id })
            .then(data => {
                // order by date desc
                data.sort(function (a, b) {
                    return b.create_date - a.create_date;
                });

                res.status(200).json(data);
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid resource ID');
    }
});

router.delete('/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Comment.deleteOne({ _id: req.params.id })
            .then(() => {
                res.status(200).json({ message: 'Comment deleted' });
            }).catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid comment ID');
    }
});

module.exports = router;
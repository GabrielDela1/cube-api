var express = require('express');
var router = express.Router();
const User = require('../models/user');
const Relation = require('../models/relation');
const sha256 = require('js-sha256');
const auth = require('../routes/isAuth.js');

//OK
router.get('/', auth, function (req, res) {
    Relation.find()
        .then(data => { res.status(200).json(data) })
        .catch(error => res.status(400).json({ error }));;
});

//OK
router.get('/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Relation.findOne({ _id: req.params.id })
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(400).json('Invalid Relation ID');
    }
});

//OK
router.post('/', auth, function (req, res) {
    var data = req.body;

    if (data.id_from != null && data.id_to != null) {
        var model = {
            id_from: data.id_from,
            id_to: data.id_to,
        };
        data.relation == null ? null : model.relation = data.relation;

        const relation = new Relation(model);

        relation.save().then(() => {
            res.status(200).json({ message: 'Relation registered' });
        }).catch((error) => {
            res.status(400).json({ error });
        });
    }
    else {
        res.status(400).json('Invalid Relation model');
    }
});


router.get('/friends/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        var Relation_id = req.params.id;

        Relation.find({ id_from: Relation_id })
            .then(relations => {
                var ids = [];
                let relationData = [];
                relations.map(data => {
                    let temp = {
                        _id: data._id,
                        id_from: data.id_from,
                        id_to: data.id_to,
                        relation: data.relation,
                    }

                    ids.push(data.id_to);

                    relationData.push(temp);
                });

                User.find({ _id: { "$in": ids } })
                    .then(data => {

                        let response = [];
                        data.map(user => {
                            let temp = {
                                id: user._id,
                                firstname: user.firstname,
                                lastname: user.lastname,
                                email: user.email,
                                tag: user.tag,
                                biography: user.biography,
                                role: user.role,
                                favorites: user.favorites,
                                avatar: user.avatar,
                            }

                            response.push(temp);
                        });

                        response.map(user => {
                            relationData.forEach(relation => {
                                if (user.id == relation.id_to) {
                                    relation.user = user;
                                }
                            });
                        });

                        res.status(200).json(relationData);
                    })
                    .catch(e => {
                        res.status(400).json(e);
                    });
            })
            .catch(e => {
                res.status(400).json(e);
            });
    }
    else {
        res.status(400).json("Invalid user id");
    }
});

//OK
router.delete('/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        Relation.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Relation deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(400).json('Invalid Relation ID');
    }
});

module.exports = router;
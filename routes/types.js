var express = require('express');
var router = express.Router();
const Type = require('../models/type');
const auth = require('../routes/isAuth.js');

router.get('/', auth, function (req, res) {
    Type.find().then(data => {
        res.status(200).json(data);
    });
});

// post

router.post('/', auth, function (req, res) {
    const type = new Type({
        name: req.body.name,
    });

    console.log(req.body);
    type.save().then(data => {
        res.status(200).json(data);
    }).catch(err => {
        res.status(500).json(err);
    });
});

module.exports = router;
var express = require('express');
var router = express.Router();
const User = require('../models/user');
const bodyParser = require('body-parser');
const sha256 = require('js-sha256');
require('dotenv').config();

const auth = require('./isAuth.js');
const admin = require('../routes/isAdmin.js');

router.get('/', function (req, res) { // auth to add
    User.find().then(data => {
        let response = [];

        data.map(user => {
            let temp = {
                id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                tag: user.tag,
                biography: user.biography,
                role: user.role,
                favorites: user.favorites,
                avatar: user.avatar,
            }

            response.push(temp);
        });

        res.status(200).json(response);
    });
});

router.get('/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.findOne({ _id: req.params.id })
            .then(user => {

                let temp = {
                    id: user._id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email,
                    tag: user.tag,
                    biography: user.biography,
                    role: user.role,
                    favorites: user.favorites,
                    avatar: user.avatar,
                }

                res.status(200).json(temp);
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});


// create user
router.post('/', function (req, res) {
    var data = req.body;
    if (data.firstname != null && data.lastname != null && data.email != null) {
        if (data.password != null || data.google_id != null) {

            var tag = '@' + data.firstname.substring(0, 2) + data.lastname.substring(0, 2) + Math.floor(Math.random() * (1000 - 100 + 1)) + 100;
            var password = data.password != null ? sha256(data.password) : null;

            const user = new User({
                firstname: data.firstname,
                lastname: data.lastname,
                tag: tag,
                biography: data.biography,
                email: data.email,
                password: password,
                google_id: data.google_id,
                avatar: "https://robohash.org/" + data.firstname.substring(0, 2) + data.lastname.substring(0, 2) + Math.floor(Math.random() * (1000 - 100 + 1)) + 100,
                age: data.age
            });
            user.save().then(() => {
                res.status(201).json({ message: 'User registered' });
            }).catch((error) => {
                res.status(400).json({ error });
            });
        }
        else {
            res.status(404).json('Invalid user model');
        }
    }
    else {
        res.status(404).json('Invalid user model');
    }
});

// put ?
router.patch('/:id', function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.findOne({ _id: req.params.id })
            .then(user => {
                if (user) {
                    if(req.body.firstname != null) {
                        user.firstname = req.body.firstname;
                    }
                    if(req.body.lastname != null) {
                        user.lastname = req.body.lastname;
                    }
                    if(req.body.password != null) {
                        user.password = sha256(req.body.password);
                    }
                    if(req.body.biography != null) {
                        user.biography = req.body.biography;
                    }
                    if(req.body.role != null) {
                        user.role = req.body.role;
                    }
                    if(req.body.age != null) {
                        user.age = req.body.age;
                    }

                    user.save().then(() => {
                        res.status(200).json({ message: 'User updated' });
                    }).catch((error) => {
                        res.status(400).json({ error });
                    });
                }
                else {
                    res.status(404).json({ error: 'User not found' });
                }
            })
            .catch(error => res.status(404).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});

router.delete('/:id', auth, function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'User deleted.' }))
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});

// add a favorite in favorites array
router.post('/:id/favorites/:resourceId', function (req, res) {
    console.log(req.params)
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.findOne({ _id: req.params.id })
            .then(data => {
                if (data.favorites.indexOf(req.params.resourceId) == -1) {
                    data.favorites.push(req.params.resourceId);
                    data.save().then(() => res.status(200).json({
                        message: 'Favorite added.',
                        success: true
                    })).catch(error => res.status(400).json({ error }));
                }
            })
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});

// remove a favorite from favorites array
router.delete('/:id/favorites/:resourceId', function (req, res) {
    if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        User.findOne({ _id: req.params.id })
            .then(data => {
                if (data.favorites.indexOf(req.params.resourceId) != -1) {
                    data.favorites.splice(data.favorites.indexOf(req.params.resourceId), 1);
                    data.save().then(() => res.status(200).json({ message: 'Favorite removed.', success: true }))
                        .catch(error => res.status(400).json({ error }));
                }
                else {
                    res.status(400).json({ message: 'Favorite does not exist.' });
                }
            })
            .catch(error => res.status(400).json({ error }));
    }
    else {
        res.status(404).json('Invalid user ID');
    }
});

module.exports = router;